#!/usr/bin/env python
__author__ = 'jamescarthew'
verbose = True
##################################################
# IMPORTS...
##################################################
from PageOperations import IndexPage, BlogPost
from argparse import ArgumentParser
import datetime
import cPickle as pickle
import os.path
##################################################
# BLOG SETTINGS
##################################################
website = 'www.jamescarthew.com/'
author = 'James Carthew'
divider_character = '<br> '

##################################################
# ARG PARSE SETTINGS
##################################################
parser = ArgumentParser(description='''
USAGE: [>python post.py newblogpost.txt]
''')

parser.add_argument('filename', help='''input file newblogpost.[txt,
html, file, whatever as long as it has ASCII]''',
                    metavar='.txt', type=str)
args = parser.parse_args()


def main():
    ##################################################
    # UTILITIES AND INITIALISATIONS
    ##################################################
    if verbose:
        print '[Main]: Utilities and Initialisations'
    now = datetime.datetime.now().strftime('%I:%M%p on %B %d, %Y')
    page = IndexPage(now, author)
    new_entry = [args.filename, website, now, author]
    original_post = True

    ##################################################
    # ARCHIVING
    ##################################################
    if verbose:
        print '[Main]: Loading Archive and Saving New Entry'

    blog_roll = []

    if os.stat('blogArchive.dat').st_size != 0:
        with open('blogArchive.dat', 'rb') as blog_archive:
            blog_roll = pickle.load(blog_archive)

    for blog in blog_roll:
        if new_entry[0] == blog[0]:
            if verbose:
                print '[Main]: New Entry duplicate exists.'
            original_post = False

    if original_post:
        blog_roll.append(new_entry)

    with open('blogArchive.dat', 'w') as blog_archive:
        pickle.dump(blog_roll, blog_archive)

    ##################################################
    # PAGE CONSTRUCTION
    ##################################################
    if verbose:
        print '[Main]: Construct HTML page'
    page.write_header()

    page.write('<div id="navbar">')

    for blog in reversed(blog_roll):
        blog = BlogPost(blog)
        blog.check_exists()
        if blog.exists:
            page.write(blog.header_link + divider_character)

    page.write('</div>')

      # page.write('<div id="blogroll">')

    for blog in reversed(blog_roll):
        blog = BlogPost(blog)
        blog.check_exists()
        if blog.exists:
            page.write(blog.blog_title)
            page.write(blog.post_content)
            page.write('<i>{!s}</i>'.format(blog.time_stamp))

    # page.write('</div>')

    page.write_footer()

if __name__ == '__main__':
    main()





