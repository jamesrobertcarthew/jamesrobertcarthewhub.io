__author__ = 'jamescarthew'


from htmlelements import HTMLElements
import os.path
from qc import wrapper

verbose = False


class IndexPage(object):

    def __init__(self, now, author):
        if verbose:
            print '[IndexPage]: New IndexPage Handle'
        self.index_html = 'index.html'
        self.page_copy = open(self.index_html, 'r+').read()
        self.page = open(self.index_html, 'w+')
        self.html = HTMLElements(author)
        self.clear_index()
        self.archive_path = '/archive/'
        self.now = now

    def clear_index(self):
        if verbose:
            print '[IndexPage]: Empty Index Page'
        self.page.truncate(0)

    def backup_index(self):
        if verbose:
            print '[IndexPage]: Back Up Index Page'
        backup_name = 'Back Up {!s}.txt'.format(self.now)
        backup_file = open(backup_name, 'w+')
        backup_file.write(self.page_copy)

    def write_header(self):
        if verbose:
            print '[IndexPage]: Write Header'
        self.page.write(self.html.html_header)

    def write_footer(self):
        if verbose:
            print '[IndexPage]: Write Footer'
        self.page.write(self.html.html_footer)

    def write(self, string):
        if verbose:
            print '[IndexPage]: Generic Write to Page'
        self.page.write(string)


class BlogPost(object):

    def __init__(self, values):    # blog_file, website, now
        if verbose:
            print '[BlogPost]: New BlogPost Handle'
        self.blog_file = values[0]
        self.website = values[1]
        self.now = values[2]
        self.html = HTMLElements(values[3])

        self.text_wrap = 100

        self.exists = True
        self.original_post = True
        self.check_exists()

        if self.exists:
            self.post_handle = open(self.blog_file, 'r')
            self.raw_title = os.path.splitext(self.blog_file)[0]
            self.blog_title = '<a name="{!s}"></a> <h2>{!s}</h2>'.format(self.raw_title.split(' ')[0],
                                                                         self.raw_title)
            self.header_link = '<a href="#{!s}"> {!s} </a>'.format(self.raw_title.split(' ')[0],
                                                                   self.raw_title.split(' ')[0])
            self.time_stamp = '<p>Posted at {!s}</p>'.format(self.now)
            self.post_content = '<p>{!s}</p>'.format(self.post_handle.read())
            self.post_content = wrapper(self.post_content, '<br>', self.text_wrap)[4:]    # Remove the first <br>

    def check_exists(self):
        if verbose:
            print '[BlogPost]: check source file exists'
        print
        self.exists = os.path.isfile(self.blog_file)


