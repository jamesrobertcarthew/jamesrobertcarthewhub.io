__author__ = 'jamescarthew'


class HTMLElements(object):

    def __init__(self, title):
        self.title = title
        self.html_header = '''
<html>
<head>
    <link rel="stylesheet" href="styles.css">
    <title>{!s}</title><h1>{!s}</h1>
</head>
<body>
'''.format(title, title)
        self.html_footer = '''
<footer> possibly the worst blogging platform on the web by <a href="http://jamescarthew.com/">james</a> </footer>
</body>
</html>
'''
        self.newline = '<br>'