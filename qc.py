#!/usr/bin/env python
__author__ = 'jamescarthew'

# the comment chunk formatter for python and C
# that delivers right to your clipboard.

# the comment
# chunk
# formatter for
# python and C
# that delivers
# right to your
# clipboard.

# QC
#
# the comment chunk formatter for python and C
# that delivers right to your clipboard.

##################################################
# QC
##################################################
# the comment chunk formatter for python and C
# that delivers right to your clipboard.
##################################################

##################################################
# QC
##################################################
# the comment chunk formatter for python and C
# that delivers right to your clipboard.
#
# hello again. now I am down here and typin in a
# scond paragraph, I might even add another.
##################################################

# from os import system
from textwrap import wrap
# import argparse
#
# parser = argparse.ArgumentParser(description='QC sends formatted chunk comments to your clipboard')
# parser.add_argument('-c','--comment', help='make a wrapped chunk comment', required=True)
# parser.add_argument('-l', '--language', help='set language - [p: python, c: c]', required=True)
# parser.add_argument('-w','--width', help='set word wrap width, default 50', required=False)
# parser.add_argument('-t','--title', help='add a (all caps) title', required=False)
# parser.add_argument('-b', '--border', help='add comment borders', dest='border',  action='store_true', required=False)
# parser.add_argument('-p', '--paragraph', help='number of paragraphs, requires subsequent input', required=False)
# args = vars(parser.parse_args())

def wrapper(wrapper_in, comment_symbol, column_width):
    wrap_list = wrap(wrapper_in, column_width - (len(comment_symbol)+1))
    wrapper_process = ''
    for i in range(0, len(wrap_list)):
        wrapper_process += comment_symbol + ' '
        wrapper_process += wrap_list[i]
        wrapper_process += '\n'
    return wrapper_process

def add_new_line(string):
    return string + '\n'

def main():
    # comment to be block-ified
    comment = args['comment']

    # required language for commenting
    if args['language'] == 'c':
        comment_symbol = '//'
    elif args['language'] == 'p':
        comment_symbol = '#'
    else:
        print("ERROR: unrecognised language argument")
        exit()

    # optionally set width of text body
    if args['width']:
        column_width = int(args['width'])
    else:
        column_width = 50

    # start to build string
    copy_to_clipboard = wrapper(comment, comment_symbol, column_width)

    if args['paragraph']:
        number_of_paragraphs = int(args['paragraph'])
        for i in range(1, number_of_paragraphs+1):
            new_paragraph = raw_input('Paragraph %s: ' % i)
            copy_to_clipboard = copy_to_clipboard + '#\n' + wrapper(new_paragraph, comment_symbol, column_width)

    # optionally add a border
    border_string = '#\n'
    if args['border']:
        border_string = ''
        for i in range(0, int(column_width) / len(comment_symbol)):
            border_string += comment_symbol
        border_string = add_new_line(border_string)
        copy_to_clipboard = copy_to_clipboard+border_string

    # optionally add an all caps title
    if args['title']:

        name = args['title']
        name = name.upper()
        name = wrapper(name, comment_symbol,  column_width)
        process = (name + border_string)
        copy_to_clipboard = process + copy_to_clipboard

    # is there any way to avoid returning to border?
    if args['border']:
        copy_to_clipboard = border_string+ copy_to_clipboard

    system("echo '%s' | pbcopy" % copy_to_clipboard)
    return

if __name__ == '__main__':
    main()

